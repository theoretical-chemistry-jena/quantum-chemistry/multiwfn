# Multiwfn
This is an inofficial gitified mirror of the Multiwfn software for wave function analysis at [http://sobereva.com/multiwfn/](http://sobereva.com/multiwfn/).

The `main` branch is an automatically updating mirror of the most recent version and contains tags for stable releases.
The `gfortran` branch contains patches to enable compilation with gfortran and tries to stay in sync with `main`.
The flake provides Fortran development tools.