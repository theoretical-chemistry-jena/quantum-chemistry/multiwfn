{
  description = "Multiwfn - A multifunctional wave function analyser";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        mwfnOvl = import ./nix/overlay.nix;
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
          overlays = [ mwfnOvl ];
        };
      in
      {
        packages.default = pkgs.multiwfn;

        # Development shell for Python, Fortran and Nix.
        devShells.default = with nixpkgs.legacyPackages."${system}";
          let
            fortDev = [ fortls fprettify gnumake ]
              ++ pkgs.multiwfn.nativeBuildInputs
              ++ pkgs.multiwfn.buildInputs
              ++ pkgs.multiwfn.propagatedBuildInputs
            ;
          in
          mkShell {
            buildInputs = [ fortDev nixpkgs-fmt ];
          };

        overlays.default = mwfnOvl;
      }
    );
}
